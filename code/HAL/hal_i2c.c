/*
 * hal_i2c.c
 *
 *  Created on: Mar 28, 2017
 *      Author: daniel
 */


#include <HAL/hal_i2c.h>

extern I2C_Com MotionSens;
uint8_t pui8Data[4];


tI2CMInstance g_sI2CMSimpleInst;
volatile bool g_bI2CMSimpleDone = true; //success flag
//
// The interrupt handler for the I2C module.
//
void I2CMSimpleIntHandler(void)
{
	//
	// Call the I2C master driver interrupt handler.
	//
	I2CMIntHandler(&g_sI2CMSimpleInst);
}
//
// The function that is provided by this example as a callback when I2C
// transactions have completed.
//
void I2CMSimpleCallback(void *pvData, uint_fast8_t ui8Status)
{
	//
	// See if an error occurred.
	//
	if(ui8Status != I2CM_STATUS_SUCCESS)
	{
	//
	// An error occurred, so handle it here if required.
	//
	}

	//
	// Indicate that the I2C transaction has completed.
	//
	g_bI2CMSimpleDone = true;
}

void HAL_InitI2C(void)
{
	// function to go Adres of I2C module needed
	SysCtlPeripheralDisable(SYSCTL_PERIPH_I2C0);
	SysCtlPeripheralReset(SYSCTL_PERIPH_I2C0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);

	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_I2C0));
	I2CMInit(&g_sI2CMSimpleInst, I2C0_BASE, INT_I2C0, 0xff, 0xff, 80000000);
	I2CIntRegister(I2C0_BASE, I2CMSimpleIntHandler);
	/*//I2CMasterEnable(I2C0_BASE);
	I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), false);
	//I2CMasterIntEnable(I2C0_BASE); // enables all Master interrupts
	//
	I2CMasterIntEnableEx(I2C0_BASE, (I2C_MASTER_INT_STOP | I2C_MASTER_INT_NACK |	I2C_MASTER_INT_TIMEOUT | I2C_MASTER_INT_DATA));
	//IntEnable(INT_I2C0);*/

	//second try, using sensorlib i2c master now
}

void HAL_I2CSendData(unsigned char address)
{

	g_bI2CMSimpleDone = false;
	I2CMWrite(&g_sI2CMSimpleInst, address, MotionSens.TxData.Data, MotionSens.TxData.len, I2CMSimpleCallback, 0);
	while(!g_bI2CMSimpleDone)
	{
	}


}

void HAL_I2CRecData(unsigned char address)
{
	//receive data

	g_bI2CMSimpleDone = false;
	I2CMRead(&g_sI2CMSimpleInst, address, MotionSens.TxData.Data, 1, MotionSens.RxData.Data, MotionSens.RxData.len,
	I2CMSimpleCallback, 0);
	while(!g_bI2CMSimpleDone)
	{
	}

}







