/*
 * hal_adc1.c
 *
 *  Created on: Nov 2, 2017
 *      Author: daniel
 */

/*
 * radar
 * AIN0 PE2
 * AIN1 PE3
 */
#include <HAL/hal_adc1.h>

extern uint32_t pui32ADC1Value[2];

void HAL_ADC1_Init_old(void)
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
	GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, RADAR_IF1|RADAR_IF2 );
	GPIOPinTypeADC(GPIO_PORTE_BASE, RADAR_IF1|RADAR_IF2);

	//sequence init
	ADCSequenceConfigure(ADC1_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
	ADCSequenceStepConfigure(ADC1_BASE, 1, 0, ADC_CTL_CH4);
	ADCSequenceStepConfigure(ADC1_BASE, 1, 1, ADC_CTL_CH2 | ADC_CTL_IE |ADC_CTL_END);
	ADCSequenceEnable(ADC1_BASE, 1);
	ADCIntClear(ADC1_BASE, 1);


}

uint8_t sampleRadarADC(void)
{

	ADCProcessorTrigger(ADC1_BASE, 1);
	while(!ADCIntStatus(ADC1_BASE, 1, false))
	{
	}
	ADCIntClear(ADC1_BASE, 1);
	ADCSequenceDataGet(ADC1_BASE,0 , pui32ADC1Value);

	return 0;

}



