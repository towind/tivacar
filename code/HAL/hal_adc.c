/*
 * hal_adc.c
 *
 *  Created on: Jun 9, 2017
 *      Author: daniel
 */

#include<HAL/hal_adc.h>


/*
 * IR Sensors
 * 1 AIN2 PE1
 * 2 AIN4 PD3
 * 3 AIN5 PD2
 *
 * radar
 * AIN0 PE2
 * AIN1 PE3
 *
 */
void HAL_InitADC()
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	//infrared
	GPIOPinTypeADC(GPIO_PORTE_BASE, IR1_SENSE_OUT);
	GPIOPinTypeADC(GPIO_PORTD_BASE, IR2_SENSE_OUT|IR3_SENSE_OUT);

	//radar
	//GPIOPinTypeADC(GPIO_PORTE_BASE, RADAR_IF1|RADAR_IF2);

	//infrared
	ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
	ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH4);
	ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_CH5);
	ADCSequenceStepConfigure(ADC0_BASE, 1, 2, ADC_CTL_CH2 | ADC_CTL_IE |ADC_CTL_END);
	ADCSequenceEnable(ADC0_BASE, 1);
	ADCIntClear(ADC0_BASE, 1);


}

unsigned int sampleADC(unsigned char chan)
{
	uint32_t pui32ADC0Value[4];
	ADCProcessorTrigger(ADC0_BASE, 1);
	while(!ADCIntStatus(ADC0_BASE, 1, false))
	{
	}
	ADCIntClear(ADC0_BASE, 1);
	ADCSequenceDataGet(ADC0_BASE, 1, pui32ADC0Value);

	return pui32ADC0Value[chan];

}


