

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "hal_h_adc1.h"
#include "hal_gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "hal_ssi2.h"
#include <math.h>

static void ADC1ISR();
static void Timer3AISR();

static uint32_t sample_count = 0;
static uint32_t IGNORE_SAMPLES; //calculate the number of ignoring samples
static uint32_t ALL_SAMPLES; //ignoring samples + valid samples
static float DAC_MULTIPLIER; //sample-counter gets multiplied with this value and gets transmitted over the spi

static uint32_t adc_radar[8];

static float radar_input1[HAL_ADC1_SAMPLES * 2];
static float radar_input2[HAL_ADC1_SAMPLES * 2];

void HAL_ADC1_Init()
{
	HAL_ADC1_input_buffer = radar_input1;
	HAL_ADC1_read_buffer_ready = 0;

	IGNORE_SAMPLES = ((HAL_ADC1_SAMPLES * HAL_ADC1_IGNORE_TIME) / (HAL_ADC1_PERIOD_TIME - HAL_ADC1_IGNORE_TIME));
	ALL_SAMPLES = (IGNORE_SAMPLES + HAL_ADC1_SAMPLES);
	DAC_MULTIPLIER = ((float)HAL_ADC1_MAX_DAC_VAL / ALL_SAMPLES);

	///*
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
	while (!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER3)) {}


	uint64_t timer_load = SysCtlClockGet();
	timer_load = timer_load * HAL_ADC1_PERIOD_TIME / ALL_SAMPLES / 1000000ULL;

	TimerConfigure(TIMER3_BASE, TIMER_CFG_PERIODIC);
	TimerLoadSet(TIMER3_BASE, TIMER_A, (uint32_t)timer_load);
	TimerControlTrigger(TIMER3_BASE, TIMER_A, true);
	TimerIntRegister(TIMER3_BASE, TIMER_A, Timer3AISR);
	IntEnable(INT_TIMER3A);
	TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
	IntMasterEnable();

	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
	while (!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC1)) {}

	ADCSequenceConfigure(ADC1_BASE, 0, ADC_TRIGGER_TIMER, 0);
	ADCSequenceStepConfigure(ADC1_BASE, 0, 0, ADC_CTL_CH1); // IF1
	ADCSequenceStepConfigure(ADC1_BASE, 0, 1, ADC_CTL_IE | ADC_CTL_END | ADC_CTL_CH0); // IF2

	ADCIntEnable(ADC1_BASE, 0);
	ADCIntRegister(ADC1_BASE, 0, ADC1ISR);

	//*/

	ADCSequenceEnable(ADC1_BASE, 0);
	TimerEnable(TIMER3_BASE, TIMER_A);
	//TimerEnable(TIMER2_BASE, TIMER_A);
	int i;
	for( i = 0; i < 2 * HAL_ADC1_SAMPLES; i++)
	{
		radar_input1[i] = 0;
		radar_input2[i] = 0;
	}
}

static void Timer3AISR()
{
	TimerIntClear(TIMER3_BASE, TIMER_A);
	//HAL_SPI2_SendWord(((uint32_t)(sample_count * DAC_MULTIPLIER)) << 2);
	HAL_RADAR_WriteCommand(((uint32_t)(sample_count * DAC_MULTIPLIER)) << 2,1);

}

static void ADC1ISR()
{
	ADCIntClear(ADC1_BASE, 0);
	ADCSequenceDataGet(ADC1_BASE, 0, adc_radar);

	if(sample_count >= IGNORE_SAMPLES)
	{

		HAL_ADC1_input_buffer[(sample_count - IGNORE_SAMPLES) * 2] = adc_radar[0];
		HAL_ADC1_input_buffer[(sample_count - IGNORE_SAMPLES) * 2 + 1] = adc_radar[1];
	}

	sample_count++;

	if(sample_count >= ALL_SAMPLES)
	{
		sample_count = 0;

		//check if buffer got read out
		if(HAL_ADC1_read_buffer_ready) return;

		HAL_ADC1_read_buffer_ready = 1;
		//switch buffers
		if(HAL_ADC1_input_buffer == radar_input1)
		{
			HAL_ADC1_input_buffer = radar_input2;
			HAL_ADC1_fft_read_buffer = radar_input1;
		}
		else
		{
			HAL_ADC1_input_buffer = radar_input1;
			HAL_ADC1_fft_read_buffer = radar_input2;
		}
	}

}

void HAL_ADC1_FreeBuffer()
{
	HAL_ADC1_read_buffer_ready = 0;
}

float * HAL_ADC1_GetInputBuffer()
{
	if(!HAL_ADC1_read_buffer_ready) return NULL;

	return HAL_ADC1_fft_read_buffer;
}

