/*
 * hal_ultrasonic.h
 *
 *  Created on: May 31, 2017
 *      Author: daniel
 */

#ifndef HAL_HAL_ULTRASONIC_H_
#define HAL_HAL_ULTRASONIC_H_

#include "driverlib/pin_map.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "driverlib/systick.h"

#include <HAL/hal_gpio.h>

#define PWM_US_FREQ 40000
#define PWM_US1 PWM_OUT_5
#define PWM_US2 PWM_OUT_4
void HAL_InitUS(void);
unsigned int HAL_MeasureUS1(unsigned char pulses);
void pwm_int(void);
void timer_int(void);
void SycTickInt();

#endif /* HAL_HAL_ULTRASONIC_H_ */
