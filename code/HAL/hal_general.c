/*
 * hal_general.c
 *
 *  Created on: 14.03.2017
 *      Author: daniel
 */

#include <HAL/hal_general.h>

USCIB1_SPICom spi_control;
I2C_Com MotionSens;
UART_Com RFModule;

void HAL_Init()
{
	HAL_UCS_Init(); //set up clock
	HAL_GPIO_Init();

	HAL_USCIB1_Init(); //setup SSI as SPI //TODO rename deprecated module name in functions (dependency at DRL!)

	HAL_PWM_Init(); //setup for throttle and steering pwm
	
	HAL_InitUart();

	HAL_InitI2C();
	//HAL_InitUS(); //Initialize Ultrasonic Module
	//HAL_InitADC();

	ssi2_init();

	//HAL_timer_init();

	//HAL_ADC1_Init(); //radar input

	HAL_ADC1_Init();

}
