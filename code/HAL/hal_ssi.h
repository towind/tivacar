#ifndef HAL_HAL_SSI_H_
#define HAL_HAL_SSI_H_


#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"

#include <HAL/hal_gpio.h>

#define SPI_CLK_FREQ 2000000 //spi clock frequency 2 MHz




void HAL_USCIB1_Init();
void HAL_USCIB1_Transmit();




void HAL_OLED_WriteCommand(unsigned char cmd);
void HAL_OLED_WriteData(unsigned char dat);



typedef struct
{
	union
	{
		unsigned char R;
		struct
		{
			unsigned char TxSuc:1; // Bit=1 wenn Daten uebertragen wurden
			unsigned char dummy:7;
		}B;
	}Status;

	struct
	{
		unsigned char len; // Länge der Daten in Bytes die übertragen werden
		unsigned char cnt;// Index auf momentan übertragene Daten
		unsigned char Data[200];// Tx Daten Array
	}TxData;


	struct
	{
		unsigned char len; // Laenge der empfangenen Daten
		unsigned char Data[200]; // Rx Daten Array
	}RxData;

}USCIB1_SPICom;

#endif
