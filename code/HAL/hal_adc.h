/*
 * hal_adc.h
 *
 *  Created on: Jun 9, 2017
 *      Author: daniel
 */

#ifndef HAL_HAL_ADC_H_
#define HAL_HAL_ADC_H_

#include "driverlib/pin_map.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "driverlib/systick.h"
#include "driverlib/adc.h"

#include <HAL/hal_gpio.h>

void HAL_InitADC();
unsigned int sampleADC(unsigned char chan);


#endif /* HAL_HAL_ADC_H_ */
