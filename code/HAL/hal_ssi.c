#include <HAL/hal_ssi.h>

/*
 * SSI1 - LCD and oled display
 * only TX from dev board!
 * SSI1TX - PF1 - MOSI
 * SSI1CLK - PF2 - CLK
 * SSI1FSS - PF3 - CS
 *
 * PA4 - RESET
 * PA3 - DATACOMMAND
 *
 */
extern USCIB1_SPICom spi_control;
void HAL_USCIB1_Init()
{
	//ssi (texas instruments version of SPI)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);
	//Configure pin mux
	GPIOPinConfigure(GPIO_PF1_SSI1TX);
	GPIOPinConfigure(GPIO_PF2_SSI1CLK);
	//GPIOPinConfigure(GPIO_PF3_SSI1FSS);

	//configure GPIO settings
	GPIOPinTypeSSI(GPIO_PORTF_BASE,LCD_SPI_MOSI|LCD_SPI_SCLK);

	//configure ssi module as spi tx module
	SSIConfigSetExpClk(SSI1_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_3,SSI_MODE_MASTER, SPI_CLK_FREQ, 8);
	SSIEnable(SSI1_BASE);

	/*//interrupt
	//processor waits anyway while transmitting
	//no need for interrupt on sending
	SSIIntEnable(SSI1_BASE,SSI_TXFF); //Interrupt on succesful send
	SSIIntRegister(SSI1_BASE,HAL_SPI_IntHandler);*/
}

void HAL_USCIB1_Transmit()
{

	for(spi_control.TxData.cnt = 0; spi_control.TxData.cnt < (spi_control.TxData.len); spi_control.TxData.cnt++)
	{
		SSIDataPut(SSI1_BASE,spi_control.TxData.Data[spi_control.TxData.cnt]);
	}

	//wait for ssi
	 while(SSIBusy(SSI1_BASE))
	{
	}
}



void HAL_OLED_WriteCommand(unsigned char cmd)
{
	OLED_RS_LO;
	OLED_CS_LO;

	SSIDataPut(SSI1_BASE, cmd);
	 while(SSIBusy(SSI1_BASE))
		{
		}
	OLED_CS_HI;

}
void HAL_OLED_WriteData(unsigned char dat)
{
	OLED_RS_HI;
	OLED_CS_LO;

	SSIDataPut(SSI1_BASE, dat);
	 while(SSIBusy(SSI1_BASE))
		{
		}
	OLED_CS_HI;

}


/*
extern void HAL_SPI_IntHandler(void)
{
	SSIIntClear(SSI1_BASE,SSI_TXFF);

	if((spi_control.TxData.cnt < spi_control.TxData.len) && spi_control.Status.B.TxSuc)
	{
		SSIDataPut(SSI1_BASE,spi_control.TxData.Data[spi_control.TxData.cnt]);
		spi_control.TxData.cnt++;
	}
	else
	{
		spi_control.Status.B.TxSuc = 0;
	}

}
*/
