/*
 * hal_i2c.h
 *
 *  Created on: Mar 28, 2017
 *      Author: daniel
 */

#ifndef HAL_HAL_I2C_H_
#define HAL_HAL_I2C_H_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/i2c.h"
#include "driverlib/interrupt.h"
#include "sensorlib/i2cm_drv.h"
#include <HAL/hal_gpio.h>




void HAL_InitI2C(void);
void HAL_I2CSendData(unsigned char address);
void HAL_I2CRecData(unsigned char address);



typedef struct
{
	unsigned char address;

	struct
	{
		int16_t X;
		int16_t Y;
		int16_t Z;
	}Accel;
	struct
	{
		float X;
		float Y;
		float Z;
	}AccelG;

	struct
	{
		int16_t X;
		int16_t Y;
		int16_t Z;
	}Gyro;
	struct
	{
		float X;
		float Y;
		float Z;
	}GyroDeg;

	float Temp;
	struct
	{
		int16_t X;
		int16_t Y;
		int16_t Z;

		int16_t asaX;
		int16_t asaY;
		int16_t asaZ;

	}Mag;
	struct
	{
		double X;
		double Y;
		double Z;
		double phi;
		float He;
	}MagD;
	unsigned char mag_addr;
	unsigned char HOFL;
	unsigned char MODE;
	unsigned char MODERES;
	struct
	{
		uint8_t len; // Länge der Daten in Bytes die übertragen werden
		uint8_t cnt;// Index auf momentan übertragene Daten
		uint8_t Data[200];// Tx Daten Array
	}TxData;


	struct
	{
		uint8_t len; // Laenge der empfangenen Daten
		uint8_t Data[200]; // Rx Daten Array
	}RxData;

}I2C_Com;

#endif /* HAL_HAL_I2C_H_ */
