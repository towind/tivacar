/*
 * hal_adc1.h
 *
 *  Created on: Nov 2, 2017
 *      Author: daniel
 */

#ifndef HAL_HAL_ADC1_H_
#define HAL_HAL_ADC1_H_

#include "driverlib/pin_map.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "driverlib/systick.h"
#include "driverlib/adc.h"

#include <HAL/hal_gpio.h>

uint32_t pui32ADC1Value[2];

void HAL_ADC1_Init_old(void);

uint8_t sampleRadarADC(void);



#endif /* HAL_HAL_ADC1_H_ */
