/*
 * hal_uart.c
 *
 *  Created on: Mar 21, 2017
 *      Author: daniel
 */

#include <HAL/hal_uart.h>

//USAGE: UARTprintf("SSI ->\n"); UARTprintf("'%c' ", c);
extern UART_Com RFModule;
void HAL_InitUart()
{
	// Configure the pin muxing for UART0 functions on port A0 and A1.
	// This step is not necessary if your part does not support pin muxing.
	// TODO: change this to select the port/pin you are using.
	//
	//
	// Select the alternate (UART) function for these pins.
	// TODO: change this to select the port/pin you are using.
	//
	GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 );

	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART0)){};
	GPIOPinConfigure(GPIO_PA0_U0RX);
	GPIOPinConfigure(GPIO_PA1_U0TX);

	// UART1 for RF Transmission
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART1)){};
	GPIOPinConfigure(GPIO_PC4_U1RX);
	GPIOPinConfigure(GPIO_PC5_U1TX);

	GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);





	//
	// Use the internal 16MHz oscillator as the UART clock source.
	//
	UARTClockSourceSet(UART0_BASE, UART_CLOCK_SYSTEM);
	UARTClockSourceSet(UART1_BASE, UART_CLOCK_SYSTEM);


	//
	// Initialize the UART for console I/O.
	//
	UARTStdioConfig(0, 115200, SysCtlClockGet());
	//UARTStdioConfig(1, 115200, SysCtlClockGet());
	UARTConfigSetExpClk(UART1_BASE, SysCtlClockGet(), 115200,(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));	// configure UART1 for RF

	//register interrupt
	UARTIntRegister(UART1_BASE ,UART1IntHandler); // Configuration of Receive Interrupt Handler

	// enable interrupts
	IntEnable(INT_UART1);
	UARTIntEnable(UART1_BASE, UART_INT_RX| UART_INT_RT); //

	//fifo is enabled with UARTIntEnable, INT triggers after RX timeout
	//UARTFIFODisable(UART1_BASE);
	//UARTFIFOEnable(UART1_BASE);
	//UARTFIFOLevelSet(UART1_BASE, UART_FIFO_TX1_8,UART_FIFO_RX1_8); //interrupt if fifo is full to specified level
}
void UART1IntHandler()
{
	//UARTCharPut(UART1_BASE,0x03);
	RFModule.RxData.len = 0;
	RFModule.RxData.IntFlag = 1;

	unsigned long ulStatus;
	ulStatus = UARTIntStatus(UART1_BASE, true);
	UARTIntClear(UART1_BASE, ulStatus);

	while(UARTCharsAvail(UART1_BASE)) //loop while there are chars
	{
		//UARTCharPut(UART1_BASE,0x05);
		RFModule.RxData.Data[RFModule.RxData.len] = UARTCharGet(UART1_BASE);
		RFModule.RxData.len++;
	}
}

void UART1send(unsigned char len)
{
	unsigned char i;

	while(UARTBusy(UART1_BASE)){}

	for(i = 0; i < len; i++)
	{
		UARTCharPut(UART1_BASE,RFModule.TxData.Data[i]);
	}
	while(UARTBusy(UART1_BASE)){}
	SysCtlDelay(1000000); //waiting 10ms after package
}

void UART1test()
{
	while(UARTBusy(UART1_BASE)){}
	UARTCharPut(UART1_BASE,0x02);
	UARTCharPut(UART1_BASE,0x12);
	while(UARTBusy(UART1_BASE)){}
	SysCtlDelay(2000000); //waiting 10ms after package
}

