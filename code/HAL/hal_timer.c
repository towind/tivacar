#include <HAL/hal_timer.h>

uint16_t cnt = 1;
extern uint32_t pui32ADC1Value[2];

void HAL_timer_init(void)
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	TimerClockSourceSet(TIMER0_BASE,TIMER_CLOCK_SYSTEM);
	IntMasterEnable();
	TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC); // 32 bit Timer

	TimerLoadSet(TIMER0_BASE,TIMER_A,1587302);


	TimerIntRegister(TIMER0_BASE, TIMER_A, Timer0Isr);    // Registering  isr
	TimerEnable(TIMER0_BASE, TIMER_A);
	IntEnable(INT_TIMER0A);
	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);


}

void Timer0Isr(void)
{

	TimerIntClear(TIMER0_BASE, TIMER_A);


	//trigger adc?
	uint16_t i = 0,j=0,k=0;
	uint16_t frame = 0;

	uint16_t adcVal[2][480];

	for(i=0;i<0xF00;i++)
	{

		frame = (i << 2)&0x3FFC;
		HAL_RADAR_WriteCommand(&frame,1);

		if((i%8)==0)
		{
			sampleRadarADC();
			adcVal[0][k] = pui32ADC1Value[0];
			adcVal[1][k] = pui32ADC1Value[1];
			k++;
		}
		else
			for(j=0;j<20;j++); //wait between 2 frames


	}
	frame = 0;
	HAL_RADAR_WriteCommand(&frame,1); // turn off
	delayMS(20);




}
