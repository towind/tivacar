/*
 * hal_uart.h
 *
 *  Created on: Mar 21, 2017
 *      Author: daniel
 */

#ifndef HAL_HAL_UART_H_
#define HAL_HAL_UART_H_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/uart.h"

#include "driverlib/pin_map.h"

#include "utils/uartstdio.h"


void HAL_InitUart();

void UART1IntHandler();

void UART1send(unsigned char data);

void UART1test();

typedef struct
{
	unsigned char address;
	unsigned char channel;

	struct
	{
		unsigned char len; // Länge der Daten in Bytes die übertragen werden
		unsigned char Data[200];// Tx Daten Array
	}TxData;

	struct
	{
		unsigned char IntFlag;
		unsigned char len; // Laenge der empfangenen Daten
		unsigned char Data[200]; // Rx Daten Array
	}RxData;

}UART_Com;

#endif /* HAL_HAL_UART_H_ */
