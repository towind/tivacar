/*
 * hal_ssi2.c
 *
 *  Created on: Oct 9, 2017
 *      Author: daniel
 */

#include <HAL/hal_ssi2.h>

/*
 * ssi2 module used for radar
 *
 * PB5 SSI2Fss RADAR_SPI_SYNC
 * PB4 SSI2Clk RADAR_SPI_SCLK
 * PB7 SSI2Tx RADAR_SPI_MOSI
 *
 *
 * PE2 AIN0 RADAR_IF1
 * PE3 AIN1 RADAR_IF2
 */

USCIB2_SPICom spi_radar;

void ssi2_init(void)
{
	//ssi (texas instruments version of SPI)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI2);

	//Configure pin mux
	GPIOPinConfigure(GPIO_PB7_SSI2TX);
	GPIOPinConfigure(GPIO_PB4_SSI2CLK);
	//GPIOPinConfigure(GPIO_PB5_SSI2FSS);

	GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_5); //oled

	//configure GPIO settings
	GPIOPinTypeSSI(GPIO_PORTB_BASE,RADAR_SPI_SCLK|RADAR_SPI_MOSI);

	//configure ssi module as spi tx module
	SSIConfigSetExpClk(SSI2_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_1,SSI_MODE_MASTER, SPI_CLK_FREQ_HI, 16);
	SSIEnable(SSI2_BASE);







}

void HAL_USCIB2_Transmit(void)
{

	for(spi_radar.TxData.cnt = 0; spi_radar.TxData.cnt < (spi_radar.TxData.len); spi_radar.TxData.cnt++)
	{
		SSIDataPut(SSI2_BASE,spi_radar.TxData.Data[spi_radar.TxData.cnt]);
	}

	//wait for ssi
	 while(SSIBusy(SSI2_BASE))
	{
	}
}
void HAL_RADAR_WriteCommand(uint16_t data, unsigned char len)
{
	uint8_t i;
	spi_radar.TxData.len = len;
	spi_radar.TxData.Data[0] = data;

	RADAR_SYNC_LO;
	HAL_USCIB2_Transmit();
	RADAR_SYNC_HI;
}

