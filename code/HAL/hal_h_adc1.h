

#ifndef HAL_HAL_ADC1_H_
#define HAL_HAL_ADC1_H_

//#include "../util/complex.h"

#define HAL_ADC1_SAMPLES 512 //samples
#define HAL_ADC1_PERIOD_TIME 400000 //time in us
#define HAL_ADC1_IGNORE_TIME 15000 //time in us
#define HAL_ADC1_MAX_DAC_VAL 3700 //number which creates an output of the DAC of ~10V


float *HAL_ADC1_input_buffer, *HAL_ADC1_fft_read_buffer;
uint8_t volatile HAL_ADC1_read_buffer_ready;

void HAL_ADC1_Init();
void HAL_ADC1_FreeBuffer();
float * HAL_ADC1_GetInputBuffer();

#endif /* HAL_HAL_ADC1_H_ */
