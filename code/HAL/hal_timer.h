/*
 * hal_timer.h
 *
 *  Created on: Oct 17, 2017
 *      Author: daniel
 */

#ifndef HAL_HAL_TIMER_H_
#define HAL_HAL_TIMER_H_


#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/pin_map.h"

#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"

#include <HAL/hal_ssi2.h>

#include <HAL/hal_gpio.h>
#include <HAL/hal_adc1.h>

void HAL_timer_init(void);


void Timer0Isr(void);
#endif /* HAL_HAL_TIMER_H_ */
