/*
 * hal_ssi2.h
 *
 *  Created on: Oct 9, 2017
 *      Author: daniel
 */

#ifndef HAL_HAL_SSI2_H_
#define HAL_HAL_SSI2_H_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/pin_map.h"

#include <HAL/hal_gpio.h>

#define SPI_CLK_FREQ_HI 5000000 //spi clock frequency 30 MHz


#define RADAR_SPI_SYNC GPIO_PIN_5
#define RADAR_SPI_SCLK GPIO_PIN_4
#define RADAR_SPI_MOSI GPIO_PIN_7
/*
 * ssi2 module used for radar
 *
 * PB5 SSI2Fss RADAR_SPI_SYNC
 * PB4 SSI2Clk RADAR_SPI_SCLK
 * PB7 SSI2Tx RADAR_SPI_MOSI
 *
 *
 * PE2 AIN0 RADAR_IF1
 * PE3 AIN1 RADAR_IF2
 */


void ssi2_init(void);
void HAL_USCIB2_Transmit(void);
void HAL_RADAR_WriteCommand(uint16_t data, unsigned char len);

typedef struct
{
	union
	{
		unsigned char R;
		struct
		{
			unsigned char TxSuc:1; // Bit=1 wenn Daten uebertragen wurden
			unsigned char dummy:7;
		}B;
	}Status;

	struct
	{
		unsigned char len; // Länge der Daten in Bytes die übertragen werden
		unsigned char cnt;// Index auf momentan übertragene Daten
		uint16_t Data[10];// Tx Daten Array
	}TxData;


	struct
	{
		unsigned char len; // Laenge der empfangenen Daten
		unsigned char Data[200]; // Rx Daten Array
	}RxData;

}USCIB2_SPICom;

#endif /* HAL_HAL_SSI2_H_ */
