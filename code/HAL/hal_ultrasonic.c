/*
 * hal_ultrasonic.c
 *
 *  Created on: May 31, 2017
 *      Author: daniel
 */

#include<HAL/hal_ultrasonic.h>


//1m 6540 100cm
//lm 5800 90cm

unsigned char pwmcounter=0;
unsigned char flag=0;
unsigned char flag2=0;

unsigned char count = 0;

float millis = 0;
unsigned int m_time =0;
void HAL_InitUS(void)
{
	IntMasterEnable();
	unsigned int us_period_count = SysCtlClockGet()/(8*PWM_US_FREQ); //period val

	//Configure PE4 & PE5 Pins as PWM
	GPIOPinConfigure(GPIO_PE4_M0PWM4);
	GPIOPinConfigure(GPIO_PE5_M0PWM5);

	PWMGenConfigure(PWM0_BASE, PWM_GEN_2, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);

	PWMGenPeriodSet(PWM0_BASE, PWM_GEN_2, us_period_count);

	PWMPulseWidthSet(PWM0_BASE, PWM_US1,25);
	PWMPulseWidthSet(PWM0_BASE, PWM_US2,25);



	PWMGenEnable(PWM0_BASE, PWM_GEN_2);
	PWMOutputState(PWM0_BASE, PWM_OUT_5_BIT, false);


	PWMIntEnable(PWM0_BASE, PWM_INT_GEN_2);

	PWMGenIntTrigEnable(PWM0_BASE, PWM_GEN_2, PWM_INT_CNT_ZERO);

	PWMGenIntRegister(PWM0_BASE, PWM_GEN_2, pwm_int);
	GPIOPinWrite(GPIO_PORTD_BASE,US1_DRIVER_EN,US1_DRIVER_EN); //enable us1
	//PB0 T2CCP0
	//timer ccp configuration
	GPIOPinTypeTimer(GPIO_PORTB_BASE, US1_SIGNAL_OUT);
	GPIOPinConfigure(GPIO_PB0_T2CCP0);
	GPIOPadConfigSet(GPIO_PORTB_BASE, GPIO_PIN_0,GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);


	TimerConfigure(TIMER2_BASE, (TIMER_CFG_SPLIT_PAIR |TIMER_CFG_A_CAP_COUNT));
	TimerControlEvent(TIMER2_BASE, TIMER_A, TIMER_EVENT_POS_EDGE);
	TimerLoadSet(TIMER2_BASE, TIMER_A, 9);
	TimerMatchSet(TIMER2_BASE, TIMER_A, 0);


	IntEnable(INT_TIMER2A);
	TimerIntRegister(TIMER2_BASE,TIMER_A,timer_int);
	TimerIntEnable(TIMER2_BASE, TIMER_CAPA_MATCH);
	TimerEnable(TIMER2_BASE, TIMER_A);

	//time measurement
	//SysTickPeriodSet(SysCtlClockGet()/1000);// for miliseconds, F_CPU/1000000 for microseconds
	//SysTickIntRegister(SycTickInt);
	//SysTickIntEnable();
	//SysTickEnable();
}

unsigned int HAL_MeasureUS1(unsigned char pulses)
{
	millis=0;

	//send 8 pulses
	PWMIntEnable(PWM0_BASE, PWM_INT_GEN_2);

	IntEnable(INT_PWM0_2);

	PWMOutputState(PWM0_BASE, PWM_OUT_5_BIT, true);
	flag = 1;

	while(flag);
	PWMOutputState(PWM0_BASE, PWM_OUT_5_BIT, false);
	PWMIntDisable(PWM0_BASE, PWM_INT_GEN_2);



	TimerConfigure(TIMER2_BASE, (TIMER_CFG_SPLIT_PAIR |TIMER_CFG_A_CAP_COUNT));
	TimerControlEvent(TIMER2_BASE, TIMER_A, TIMER_EVENT_POS_EDGE);
	TimerLoadSet(TIMER2_BASE, TIMER_A, 5);
	TimerMatchSet(TIMER2_BASE, TIMER_A, 0);


	IntEnable(INT_TIMER2A);
	TimerIntRegister(TIMER2_BASE,TIMER_A,timer_int);
	TimerIntEnable(TIMER2_BASE, TIMER_CAPA_MATCH);
	TimerEnable(TIMER2_BASE, TIMER_A);

	flag2 = 1;
	while(flag2)
	{
		millis++;
		if(millis>12000)
			return 0;
	}
	return (unsigned int)((millis));
	//return (unsigned int)millis;
}

void pwm_int(void)
{

	PWMGenIntClear(PWM0_BASE, PWM_GEN_2, PWM_INT_CNT_ZERO);

	pwmcounter++;

	if(pwmcounter>=15)
	{


		pwmcounter=0;
		flag=0;

	}

}

void timer_int(void)
{

	TimerIntClear(TIMER2_BASE, TIMER_CAPA_MATCH);

	count++;
	flag2=0;


}


