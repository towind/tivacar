/*
 * AL_RF.h
 *
 *  Created on: May 5, 2017
 *      Author: daniel
 */

#ifndef AL_AL_RF_H_
#define AL_AL_RF_H_

#include <HAL/hal_general.h>
#include <DRL/drl_general.h>

void RFHandler();
void SendData(unsigned int dat2,unsigned int dat3);

#endif /* AL_AL_RF_H_ */
