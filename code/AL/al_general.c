/*
 * al_general.c
 *
 *  Created on: Mar 16, 2017
 *      Author: daniel
 */
#include <AL/al_general.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

extern UART_Com RFModule;
extern I2C_Com MotionSens;

char pstring[80];
char pstring2[80];
char pstring3[80];
char pstring4[80];
char pstring5[80];
char pstring6[80];
char pstring7[80];

void AL_Init()
{
	//nothing yet
}

void AL_test(void)
{
	DRL_ReadMotionSensorData();

	sprintf(pstring,"%f",MotionSens.AccelG.X);
	sprintf(pstring2,"%f",MotionSens.AccelG.Y);
	sprintf(pstring3,"%f",MotionSens.AccelG.Z);
	sprintf(pstring4,"%f",MotionSens.GyroDeg.X);
	sprintf(pstring5,"%f",MotionSens.GyroDeg.Y);
	sprintf(pstring6,"%f",MotionSens.GyroDeg.Z);
	sprintf(pstring7,"%f",MotionSens.MagD.phi);
	/*UARTprintf("%s;", pstring);
	UARTprintf("%s;", pstring2);
	UARTprintf("%s;", pstring3);
	UARTprintf("%s;", pstring4);
	UARTprintf("%s;", pstring5);
	UARTprintf("%s;", pstring6);*/
	UARTprintf("%s\n", pstring7);
	//UARTprintf("%d;%d;%d\n",MotionSens.Mag.X,MotionSens.Mag.Y,MotionSens.Mag.Z);
	//UARTprintf("%d;%d;%d\n",MotionSens.Gyro.X,MotionSens.Gyro.Y,MotionSens.Gyro.Z);
	//UARTprintf("Accel: X:%d Y:%d Z:%d\n",MotionSens.Accel.X,MotionSens.Accel.Y,MotionSens.Accel.Z);
	//UARTprintf("Gyro: X:%d Y:%d Z:%d\n",MotionSens.Gyro.X,MotionSens.Gyro.Y,MotionSens.Gyro.Z);
	//UARTprintf("temp: %f\n",MotionSens.Temp);


	/*DRL_LCD_WriteUInt(MotionSens.Accel.X, 0, 0);
	DRL_LCD_WriteUInt(MotionSens.Accel.Y, 1, 0);
	DRL_LCD_WriteUInt(MotionSens.Accel.Z, 2, 0);
	DRL_LCD_WriteUInt(MotionSens.Gyro.X, 3, 0);
	DRL_LCD_WriteUInt(MotionSens.Gyro.Y, 4, 0);
	DRL_LCD_WriteUInt(MotionSens.Gyro.Z, 5, 0);
	DRL_LCD_WriteUInt(MotionSens.Temp, 5, 0);*/
	delayMS(100);
	/*
	if(MotionSens.Accel.X < 51000 && MotionSens.Accel.X > 45000)
		LCD_BACKLIGHT_ON;
	else
		LCD_BACKLIGHT_OFF;

	*/
}
