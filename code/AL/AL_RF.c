/*
 * AL_RF.c
 *
 *  Created on: May 5, 2017
 *      Author: daniel
 */
#include <AL/AL_RF.h>
#include <HAL/hal_gpio.h>
#include <DRL/drl_lcd.h>
extern UART_Com RFModule;

#define NUM_SAMPLES 640
char input_data[NUM_SAMPLES] =  {
     #include "sinus-6.txt"
};        // input data

unsigned char send1_sequence[4] = {0x02,0x020,0x12,0x05};
unsigned int j = 0;
unsigned char reverseflag = 0;
void RFHandler()
{

	unsigned int steer, thrott,throttprev;

	if(RFModule.RxData.IntFlag)
	{


		RFModule.RxData.IntFlag = 0;
		if(UART1_BASE,RFModule.RxData.Data[0] == 0x02 && UART1_BASE,RFModule.RxData.Data[1] == 0x20 && UART1_BASE,RFModule.RxData.Data[2] == 0x12)
		{
			steer = (int)(RFModule.RxData.Data[5]-100);
			DRL_SetSteering(steer);
			thrott = (int)(RFModule.RxData.Data[4]-100);
			if(thrott < 0 && throttprev >=0)
			{
				DRL_SetThrottle(0);
				delayMS(50);
				DRL_SetThrottle(thrott);
				DRL_SetThrottle(0);

			}
			else
				DRL_SetThrottle(thrott);
			throttprev=thrott;
			//UARTprintf("%d;%d\n",(int)(RFModule.RxData.Data[5]-100),(int)(RFModule.RxData.Data[4]-100));

		}

	}


}

void SendData(unsigned int dat2,unsigned int dat3)
{
	unsigned char i,j=0,len=0,len2=0;
	char out[16];

	char out2[16];

	for(i = 0; i < 3; i++)
	{
		RFModule.TxData.Data[i] = send1_sequence[i];
	}

	itoa(dat2,out,10);
	i=0;
	while(out[i] != '\0')
	{
		len++;
		i++;
	}
	RFModule.TxData.Data[3] = len+1;
	for(i=4;i<len+4;i++)
	{
		RFModule.TxData.Data[i] = out[j];
		j++;
	}

	//second data
	itoa(dat3,out2,10);
	i=0;
	while(out2[i] != '\0')
	{
		len2++;
		i++;
	}
	RFModule.TxData.Data[3] = RFModule.TxData.Data[3]+len2+1;
	j=0;
	RFModule.TxData.Data[len+4] = ';';
	for(i=5+len;i<len+len2+5;i++)
	{
		RFModule.TxData.Data[i] = out2[j];
		j++;
	}



	RFModule.TxData.Data[len+len2+5] = '\n';


	DRL_radioTX(len+len2+6);

	//UARTprintf("%d;%d;%d;%d\n",sampleADC(0),sampleADC(1),sampleADC(2),HAL_MeasureUS1(8));
}


