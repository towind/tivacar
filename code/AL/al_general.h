/*
 * al_general.h
 *
 *  Created on: Mar 16, 2017
 *      Author: daniel
 */

#ifndef AL_AL_GENERAL_H_
#define AL_AL_GENERAL_H_

#include <HAL/hal_general.h>
#include <DRL/drl_general.h>
#include <AL/AL_RF.h>

void AL_Init();


#endif /* AL_AL_GENERAL_H_ */
