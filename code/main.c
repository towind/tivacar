/*
 * main.c
 *
 *  Created on: 14.03.2017
 *      Author: daniel@heger.me
 *      		florian schlagbauer
 *      FH Joanneum ECE15
 *  crazy car code from MSP430 to tiva
 */

/*
 * general TODO
 *
 * display drl
 * gpio activations
 * 	(timers)
 * 	Interrupts (Start stop buttons on tiva launchpad)
 *CLEAN UP CODE (coding conventions and deprecated function names in drl/hal)
 *import driving algo
 */

//lib includes

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"


//this is stupid
#include "utils/uartstdio.h"
#include "utils/uartstdio.c"

#include "sensorlib/i2cm_drv.h"
#include "sensorlib/i2cm_drv.c"

//main layer includes
//all other layer files are included in those
#include <HAL/hal_general.h>
#include <DRL/drl_general.h>
#include <AL/al_general.h>

//pictures

#include <DRL/drl_oled_pictures.h>

#define _EVER ;;

#define SEND 0
#define REC 1

//global comm struct vars
extern USCIB1_SPICom spi_control;

extern I2C_Com MotionSens;

extern UART_Com RFModule;

unsigned int ultrasonic_dist=0,adc1=0,adc2=0,adc3=0;
int main(void)
{
	//Init all Layers
	HAL_Init();
	DRL_Init();
	AL_Init();

	/*
	 * FWD
	 * RWD
	 * 0
	 * RWD
	 */

	/*DRL_SetSteering(0);
	delayMS(2000);

	DRL_SetThrottle(15);
	delayMS(2000);
	DRL_SetThrottle(-15);
	delayMS(2000);
	DRL_SetThrottle(0);
	delayMS(2000);
	DRL_SetThrottle(-15);
	delayMS(2000);
	DRL_SetThrottle(0);
*/
	DRL_SetSteering(0);
	DRL_SetThrottle(0);
	//DRL_LCD_WriteString("ECE15 rulz", 10, 7, 0);


	UARTprintf("setup finished\n");


	LCD_BACKLIGHT_ON;


	for(_EVER)
	{
		/*if(SEND)
		{
			ultrasonic_dist = 0;
			adc1 = sampleADC(0);
			adc2 = sampleADC(1);
			adc3 = sampleADC(2);

			DRL_LCD_WriteUInt(ultrasonic_dist,0,0);
			DRL_LCD_WriteUInt(adc1,1,0);
			DRL_LCD_WriteUInt(adc2,2,0);
			DRL_LCD_WriteUInt(adc3,3,0);


			SendData(adc1,adc2);


		}
		if(REC)
			RFHandler();*/





	}

}


