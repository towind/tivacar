/*
 * driver_general.h
 *
 *  Created on: 31.10.2016
 *      Author: daniel
 */

#ifndef DRL_DRL_GENERAL_H_
#define DRL_DRL_GENERAL_H_

#include <DRL/drl_aktorik.h>
#include <DRL/drl_lcd.h>
#include <DRL/drl_oled.h>
#include <DRL/drl_i2c.h>
#include <DRL/drl_radio.h>

void DRL_Init(void);


#endif /* DRL_DRL_GENERAL_H_ */
