/*
 * driver_aktorik.h
 *
 *  Created on: 31.10.2016
 *      Author: daniel
 */

#ifndef DRL_DRL_AKTORIK_H_
#define DRL_DRL_AKTORIK_H_



#define HALT_TIME_MS 5

#define MAX_STEERING_LEFT 2300
#define MAX_STEERING_RIGHT 3800
#define STEERING_MIDDLE 3050

#define MAX_THROTTLE_BPW 2000
#define MIN_THROTTLE_BPW 2950
#define MIN_THROTTLE_FPW 3050
#define MAX_THROTTLE_FPW 4000
#define MAX_BRAKE 3000

void DRL_ESCSetup(void);
unsigned int DRL_SetSteering(int steering); // -100 = full left, 0 = middle, 100 = full right

unsigned int DRL_SetThrottle(int throttle); // -100 = full backward 100 full forward


#endif /* DRL_DRL_AKTORIK_H_ */
