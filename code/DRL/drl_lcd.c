/*
 * drl_lcd.c
 *
 *  Created on: 14.11.2016
 *      Author: daniel
 */



#include <DRL/drl_lcd.h>
#include <DRL/drl_lcd_fonts.h>


extern USCIB1_SPICom spi_control;


void DRL_LCD_Init()
{
	unsigned char i;

	unsigned char cmdAry[12] = {LCD_RST,LCD_BIAS,ADC_SEL_NORMAL,RES_RATIO,ELE_VOL_MODE,ELE_VOL_VALUE,POWER_CONT,DISPLAY_ON, DISPLAY_ALL_POINTS, DISPLAY_ALL_POINTS_OFF,RST_TOPLEFT};

	// reset lcd from previously set parameters
	LCD_RESET_LO;
	delayMS(1);			// wait for display
	LCD_RESET_HI;

	LCD_CS_HI; //Chip select HI

	for(i=1; i < 8;i++)
	{
		DRL_LCD_WriteCommand(&cmdAry[i],1);

	}

	DRL_LCD_WriteCommand(&cmdAry[10],1); //reset to top left
	DRL_LCD_Clear();
	LCD_BACKLIGHT_ON;				// turn on display backlight

	//DRL_LCD_WriteCommand(&cmdAry[8],1); //test, display all points



}
void DRL_LCD_Clear(void) //clears display ram //this could be done prettier
{
	unsigned char i,j;
	unsigned char pagecursor = 0xB0; //page adress set
	unsigned char colcursorA = 0x10; //col adress set upper bit
	unsigned char colcursorB = 0x00; //col adress set lower bit
	unsigned char cursorVal = 0x00; //val of cursor on page (0-132)


	for(i = 0; i < RAM_PAGES; i++) //cycle through all RAM pages
	{
		pagecursor = pagecursor | i;
		DRL_LCD_WriteCommand(&pagecursor, 1);
		DRL_LCD_WriteCommand(&colcursorA, 1);
		DRL_LCD_WriteCommand(&colcursorB, 1);
		for(j=0; j < DISPLAYWIDTH; j++) //cycle through all columns
		{
			DRL_LCD_WriteData(&cursorVal, 1);
		}
		pagecursor = 0xB0;
	}
}
void DRL_LCD_ClearPage(unsigned char page)
{
	unsigned char j;
	unsigned char pagecursor = 0xB0; //page adress set
	unsigned char colcursorA = 0x10; //col adress set upper bit
	unsigned char colcursorB = 0x00; //col adress set lower bit
	unsigned char cursorVal = 0x00; //val of cursor on page (0-132)


	pagecursor = pagecursor | page;
	DRL_LCD_WriteCommand(&pagecursor, 1);
	DRL_LCD_WriteCommand(&colcursorA, 1);
	DRL_LCD_WriteCommand(&colcursorB, 1);
	for(j=0; j < DISPLAYWIDTH; j++) //cycle through all columns
	{
		DRL_LCD_WriteData(&cursorVal, 1);
	}

}
void DRL_LCD_SetPosition(unsigned char page, unsigned char col)
{
	unsigned char x = (0xB0|(page));
	unsigned char y = (0x10|(col >> 4));
	unsigned char y2 = (0x00|(col));


	DRL_LCD_WriteCommand(&x, 1); //set page cursor


	DRL_LCD_WriteCommand(&y,1); //set col cursor first byte
	DRL_LCD_WriteCommand(&y2,1); //col cursor second byte
}
void DRL_LCD_WriteCommand (unsigned char *data, unsigned char len)
{
	LCD_DATACMD_LO; //enter datacommand mode
	LCD_CS_LO;
	int i;
	spi_control.TxData.len = len;
	for(i = 0; i < len; i++)
	{
		spi_control.TxData.Data[i] = *data++;
	}
	HAL_USCIB1_Transmit();
	LCD_CS_HI;
	LCD_DATACMD_HI;

}

void DRL_LCD_WriteAry(unsigned char (*ary)[9]) //just for arrays 132x64 (max resolution)
{
	unsigned char i;
	DRL_LCD_Clear();

	for(i = 0; i < RAM_PAGES;i++)
	{
		DRL_LCD_SetPosition(i,0);

		DRL_LCD_WriteData(ary[i], 9);
	}
}


void DRL_LCD_WriteString(unsigned char *str, unsigned char len, unsigned char page, unsigned char col)
{
	unsigned char i,j;


	DRL_LCD_ClearPage(page);
	DRL_LCD_SetPosition(page,col);

	//LCD_LSB_FIRST; //toggle spi sending mode
	LCD_DATACMD_HI;
	LCD_CS_LO;

	spi_control.TxData.len = len*CHAR_WIDTH; //

	for(i = 0; i < len; i++)
	{
		for(j = 0; j< CHAR_WIDTH; j++)
		{
			spi_control.TxData.Data[i*6+j] = font[*str-32][j];
		}
		str++;
	}

	HAL_USCIB1_Transmit();

	LCD_CS_HI; //leave everything as found
	LCD_DATACMD_LO;
	//LCD_MSB_FIRST;
}

void DRL_LCD_WriteUInt(unsigned int num, unsigned char page, unsigned char col)
{
	unsigned char i=0,j,len=0;
	char out[16];
	char len_total = 5; //integer is max 65535, so max 5 numbers

	//DRL_LCD_ClearPage(page);
	DRL_LCD_SetPosition(page,col);
	//LCD_LSB_FIRST;
	LCD_DATACMD_HI;
	LCD_CS_LO;
	spi_control.TxData.len = 0;
	itoa(num,out,10);
	while(out[i] != '\0')
	{
		len++;
		i++;
	}
	char leadzeros = len_total-len; //leading 0 will be displayed as spaces
	spi_control.TxData.len = len_total*CHAR_WIDTH; //always send 5 characters
	for(i=0;i<leadzeros;i++)
	{
		for(j = 0; j< CHAR_WIDTH; j++)
		{
			spi_control.TxData.Data[i*6+j] = font[0][j]; //space
		}
	}
	for(i = leadzeros; i < len_total; i++)
	{
		for(j = 0; j< CHAR_WIDTH; j++)
		{
			spi_control.TxData.Data[i*6+j] = font[out[i-leadzeros]-32][j];
		}
	}

	HAL_USCIB1_Transmit();
	LCD_CS_HI;
	LCD_DATACMD_LO;
	//LCD_MSB_FIRST;
}

void DRL_LCD_WriteUIntwLabel(unsigned char *str, unsigned char labelLen,unsigned int num, unsigned char page, unsigned char col)
{
	unsigned char i=0,j,k=0,len=0;
	char out[16];

	DRL_LCD_ClearPage(page);
	DRL_LCD_SetPosition(page,col);
	//LCD_LSB_FIRST;
	LCD_DATACMD_HI;
	LCD_CS_LO;
	spi_control.TxData.len = 0;
	itoa(num,out,10);
	while(out[i] != '\0')
	{
		len++;
		i++;
	}
	spi_control.TxData.len = len*CHAR_WIDTH+labelLen*CHAR_WIDTH;
	for(i = 0; i < labelLen; i++) //label to buf
	{
		for(j = 0; j< CHAR_WIDTH; j++)
		{
			spi_control.TxData.Data[i*6+j] = font[*str-32][j];
		}
		str++;
	}
	for(i = labelLen; i < (len+labelLen); i++) //int to buffer
	{
		for(j = 0; j< CHAR_WIDTH; j++)
		{
			spi_control.TxData.Data[i*6+j] = font[out[k]-32][j];

		}
		k++;
	}

	HAL_USCIB1_Transmit();
	LCD_CS_HI;
	LCD_DATACMD_LO;
	//LCD_MSB_FIRST;
}

void DRL_LCD_WriteData (unsigned char *data, unsigned char len) //test function
{
	//LCD_LSB_FIRST;
	LCD_DATACMD_HI;
	LCD_CS_LO;
	int i;
	spi_control.TxData.len = len;
	for(i = 0; i < len; i++)
	{
		spi_control.TxData.Data[i] = *data++;
	}

	HAL_USCIB1_Transmit();
	LCD_CS_HI;
	LCD_DATACMD_LO;
	//LCD_MSB_FIRST;

}


void DRL_LCD_WriteA(void) //test function
{
	/*//unsigned char A[6] = { 0x01, 0x7C, 0x12, 0x11, 0x12, 0x7C };
	unsigned char A[6][2] = {{0x7C, 0x12, 0x11, 0x12, 0x7C,0x00 },{0x7C, 0x12, 0x11, 0x12, 0x7C,0x00 }};
	unsigned char cmd = 0x0F;
	unsigned char buf[12];

	unsigned char cmd1 = 0xB0; //page adress set
	unsigned char j = 0x10; //col adress set upper bit
	unsigned char i = 0x00; //col adress set lower bit

	for (i = 0; i < 2; i++)
	{
		for(j=0; j < 6; j++)
		{
			buf[i*6+j] = font[65][j];
		}
	}
	DRL_LCD_WriteData(buf,12); //this didnt work as expected
	//DRL_LCD_WriteData(A[1],6);
*/


}
void itoa(unsigned int value, char* result, unsigned char base)
{
  // check that the base if valid
  if (base < 2 || base > 36) { *result = '\0';}

  char* ptr = result, *ptr1 = result, tmp_char;
  int tmp_value;

  do {
	tmp_value = value;
	value /= base;
	*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
  } while ( value );

  // Apply negative sign
  if (tmp_value < 0) *ptr++ = '-';
  *ptr-- = '\0';
  while(ptr1 < ptr) {
	tmp_char = *ptr;
	*ptr--= *ptr1;
	*ptr1++ = tmp_char;
  }

}
