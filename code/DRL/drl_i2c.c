/*
 * drl_i2c.c
 *
 *  Created on: Mar 28, 2017
 *      Author: daniel
 */
#include <DRL/drl_i2c.h>
#include <math.h>
#define PI 3.1416

extern I2C_Com MotionSens;

void DRL_InitMotionSens(void)
{
	//MPU_9250_9AxisMotion Sensor
	MotionSens.address = 0b1101000; //AD0 sets LSB, see schematic 0x63

	MotionSens.TxData.len = 2;
	MotionSens.TxData.Data[0] = 0x6B; //reset
	MotionSens.TxData.Data[1] = 0x00;
	HAL_I2CSendData(MotionSens.address);
	SysCtlDelay(100000);



	MotionSens.TxData.Data[0] = 0x1D; //lowpass filter
	MotionSens.TxData.Data[1] = 0x06;
	HAL_I2CSendData(MotionSens.address);
	SysCtlDelay(100000);


	MotionSens.TxData.Data[0] = 0x1C; //+- 4G sensitivity
	MotionSens.TxData.Data[1] = 0b01000;
	HAL_I2CSendData(MotionSens.address);
	SysCtlDelay(100000);

	MotionSens.TxData.Data[0] = 0x1C;
	MotionSens.TxData.len = 1;
	MotionSens.RxData.len = 1;
	HAL_I2CRecData(MotionSens.address);
	//Gyro
	MotionSens.TxData.Data[0] = 0x1A; //bandwidth 5 Hz
	MotionSens.TxData.Data[1] = 0b110;
	HAL_I2CSendData(MotionSens.address);
	SysCtlDelay(100000);



	MotionSens.TxData.Data[0] = 0x1B; //1000 deg /sec
	MotionSens.TxData.Data[1] = 0b11||0b10000;
	HAL_I2CSendData(MotionSens.address);
	SysCtlDelay(100000);

	MotionSens.TxData.Data[0] = 0x37; //enable magnetometer bypass (make address visible on iic bus)
	MotionSens.TxData.Data[1] = 0b1;
	HAL_I2CSendData(MotionSens.address);
	SysCtlDelay(100000);

	MotionSens.mag_addr = 0x0C;



	/*MotionSens.TxData.Data[0] = 0x0A; //magnetometer continous mode
	MotionSens.TxData.Data[1] = 0b0100;
	HAL_I2CSendData(MotionSens.address);
	SysCtlDelay(100000);*/

	//read magnetometer adjustment
	MotionSens.TxData.Data[0] = 0x10;
	MotionSens.TxData.len = 1;
	MotionSens.RxData.len = 3;
	HAL_I2CRecData(MotionSens.mag_addr);
	MotionSens.Mag.asaX = MotionSens.RxData.Data[0];
	MotionSens.Mag.asaY = MotionSens.RxData.Data[1];
	MotionSens.Mag.asaZ = MotionSens.RxData.Data[2];

	MotionSens.TxData.Data[0] = 0x0A;
	MotionSens.TxData.len = 1;
	MotionSens.RxData.len = 1;
	HAL_I2CRecData(MotionSens.mag_addr);
	MotionSens.MODE = MotionSens.RxData.Data[0];

}

void DRL_ReadMotionSensorData(void)
{
	float resolution = 8.0 / 65535.0;
	float resolutionGyro = 1000.0/65535.0;
	float resolutionMag = 4912.0 /32766.0;

	//does it autoincrement registers?
	//then this could work
	MotionSens.TxData.Data[0] = ACCX_H;
	MotionSens.TxData.len = 1;
	MotionSens.RxData.len = 14;
	HAL_I2CRecData(MotionSens.address);

	//accel
	MotionSens.Accel.X = (int16_t)((MotionSens.RxData.Data[0]<<8)|MotionSens.RxData.Data[1]);
	MotionSens.AccelG.X= resolution * (float)MotionSens.Accel.X;
	MotionSens.Accel.Y = (int16_t)((MotionSens.RxData.Data[2]<<8)|MotionSens.RxData.Data[3]);
	MotionSens.AccelG.Y= resolution * (float)MotionSens.Accel.Y;
	MotionSens.Accel.Z = (int16_t)((MotionSens.RxData.Data[4]<<8)|MotionSens.RxData.Data[5]);
	MotionSens.AccelG.Z= resolution * (float)MotionSens.Accel.Z;
	//temp
	MotionSens.Temp = (float)((uint16_t)(MotionSens.RxData.Data[6]<<8)|MotionSens.RxData.Data[7])/333.87 +21;
	//gyro
	MotionSens.Gyro.X = ((int16_t)(MotionSens.RxData.Data[8]<<8)|MotionSens.RxData.Data[9]);
	MotionSens.GyroDeg.X = resolutionGyro * (float)MotionSens.Gyro.X;
	MotionSens.Gyro.Y = ((int16_t)(MotionSens.RxData.Data[10]<<8)|MotionSens.RxData.Data[11]);
	MotionSens.GyroDeg.Y = resolutionGyro * (float)MotionSens.Gyro.Y;
	MotionSens.Gyro.Z = ((int16_t)(MotionSens.RxData.Data[12]<<8)|MotionSens.RxData.Data[13]);
	MotionSens.GyroDeg.Z = resolutionGyro * (float)MotionSens.Gyro.Z;

	//read magnetometer
	MotionSens.TxData.Data[0] = 0x03;
	MotionSens.TxData.len = 1;
	MotionSens.RxData.len = 6;
	HAL_I2CRecData(MotionSens.mag_addr);
	MotionSens.Mag.X = (int16_t)((MotionSens.RxData.Data[1]<<8)|MotionSens.RxData.Data[0]);
	MotionSens.Mag.Y = (int16_t)((MotionSens.RxData.Data[3]<<8)|MotionSens.RxData.Data[2]);
	MotionSens.Mag.Z = (int16_t)((MotionSens.RxData.Data[5]<<8)|MotionSens.RxData.Data[4]);


	MotionSens.TxData.Data[0] = 0x09;
	MotionSens.TxData.len = 1;
	MotionSens.RxData.len = 6;
	HAL_I2CRecData(MotionSens.mag_addr);
	MotionSens.HOFL = MotionSens.RxData.Data[0];

	//resolution magnetometer adjustment
	MotionSens.MagD.X = (double)((((double)MotionSens.Mag.asaX - 128.0)*0.5)/128.0 +1.0)* (double)MotionSens.Mag.X;
	MotionSens.MagD.Y = (double)((((double)MotionSens.Mag.asaY - 128.0)*0.5)/128.0 +1.0)* (double)MotionSens.Mag.Y;
	MotionSens.MagD.Z = (double)((((double)MotionSens.Mag.asaZ - 128.0)*0.5)/128.0 +1.0)* (double)MotionSens.Mag.Z;

	//MotionSens.MagD.X = resolutionMag * MotionSens.MagD.X;
	//MotionSens.MagD.Y = resolutionMag * MotionSens.MagD.Y;
	//MotionSens.MagD.Z = resolutionMag * MotionSens.MagD.Z;

	MotionSens.MagD.phi = -atan2(MotionSens.MagD.X,MotionSens.MagD.Y) * (180.0/PI);

}





