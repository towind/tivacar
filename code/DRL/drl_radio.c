/*
 * drl_radio.c
 *
 *  Created on: Apr 26, 2017
 *      Author: daniel
 */
#include <DRL/drl_radio.h>



extern UART_Com RFModule;

void DRL_InitRFModule()
{
	RFModule.channel = 0x05;
	RFModule.address = 0x10;


	//set address
	RFModule.TxData.Data[0] = 0x02;
	RFModule.TxData.Data[1] = 0x10;
	RFModule.TxData.Data[2] = RFModule.address;
	DRL_radioTX(3);


	//set channel
	RFModule.TxData.Data[0] = 0x02;
	RFModule.TxData.Data[1] = 0x14;
	RFModule.TxData.Data[2] = RFModule.channel;
	DRL_radioTX(3);

	/*//read address
	RFModule.TxData.Data[0] = 0x02;
	RFModule.TxData.Data[1] = 0x11;
	DRL_radioTX(2);


	//read channel
	RFModule.TxData.Data[0] = 0x02;
	RFModule.TxData.Data[1] = 0x13;
	DRL_radioTX(2);*/

}

void DRL_radioRX()
{

}
void DRL_radioTX(unsigned char len) //communicates with hal
{
	UART1send(len);
}
