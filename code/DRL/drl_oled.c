/*
 * drl_oled.c
 *
 *  Created on: Mar 23, 2017
 *      Author: daniel
 */


#include <DRL/drl_oled.h>


void DRL_Oled_Init()
{

	OLED_CS_LO;
	OLED_RS_LO;

	//reset init
	//toggle a bit
	OLED_RST_HI;
	delayMS(500);
	OLED_RST_LO;
	delayMS(500);
	OLED_RST_HI;
	delayMS(500);

	//initialisation sequence
	//taken directly from adafruit

	HAL_OLED_WriteCommand(SSD1331_CMD_DISPLAYOFF);  	// 0xAE
	HAL_OLED_WriteCommand(SSD1331_CMD_SETREMAP); 	// 0xA0
	HAL_OLED_WriteCommand(0x72);				// RGB Color
	HAL_OLED_WriteCommand(SSD1331_CMD_STARTLINE); 	// 0xA1
	HAL_OLED_WriteCommand(0x0);
	HAL_OLED_WriteCommand(SSD1331_CMD_DISPLAYOFFSET); 	// 0xA2
	HAL_OLED_WriteCommand(0x0);
	HAL_OLED_WriteCommand(SSD1331_CMD_NORMALDISPLAY);  	// 0xA4
	HAL_OLED_WriteCommand(SSD1331_CMD_SETMULTIPLEX); 	// 0xA8
	HAL_OLED_WriteCommand(0x3F);  			// 0x3F 1/64 duty
	HAL_OLED_WriteCommand(SSD1331_CMD_SETMASTER);  	// 0xAD
	HAL_OLED_WriteCommand(0x8E);
	HAL_OLED_WriteCommand(SSD1331_CMD_POWERMODE);  	// 0xB0
	HAL_OLED_WriteCommand(0x0B);
	HAL_OLED_WriteCommand(SSD1331_CMD_PRECHARGE);  	// 0xB1
	HAL_OLED_WriteCommand(0x31);
	HAL_OLED_WriteCommand(SSD1331_CMD_CLOCKDIV);  	// 0xB3
	HAL_OLED_WriteCommand(0xF0);  // 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)
	HAL_OLED_WriteCommand(SSD1331_CMD_PRECHARGEA);  	// 0x8A
	HAL_OLED_WriteCommand(0x64);
	HAL_OLED_WriteCommand(SSD1331_CMD_PRECHARGEB);  	// 0x8B
	HAL_OLED_WriteCommand(0x78);
	HAL_OLED_WriteCommand(SSD1331_CMD_PRECHARGEA);  	// 0x8C
	HAL_OLED_WriteCommand(0x64);
	HAL_OLED_WriteCommand(SSD1331_CMD_PRECHARGELEVEL);  	// 0xBB
	HAL_OLED_WriteCommand(0x3A);
	HAL_OLED_WriteCommand(SSD1331_CMD_VCOMH);  		// 0xBE
	HAL_OLED_WriteCommand(0x3E);
	HAL_OLED_WriteCommand(SSD1331_CMD_MASTERCURRENT);  	// 0x87
	HAL_OLED_WriteCommand(0x06);
	HAL_OLED_WriteCommand(SSD1331_CMD_CONTRASTA);  	// 0x81
	HAL_OLED_WriteCommand(0x91);
	HAL_OLED_WriteCommand(SSD1331_CMD_CONTRASTB);  	// 0x82
	HAL_OLED_WriteCommand(0x50);
	HAL_OLED_WriteCommand(SSD1331_CMD_CONTRASTC);  	// 0x83
	HAL_OLED_WriteCommand(0x7D);
	HAL_OLED_WriteCommand(SSD1331_CMD_DISPLAYON);	//--turn on oled panel


}



void DRL_OLED_PushPic(unsigned char *pic,uint16_t colorA,uint16_t colorB)
{
	DRL_OLED_SetPosition(0,0);
	int i,j;
	for(i=0;i<768;i++)
	{
		for(j=7;j>=0;j--)
		{
			if(pic[i]&(1<<j))
				DRL_OLED_PushColor(colorA);
			else
				DRL_OLED_PushColor(colorB);
		}
	}
}

void DRL_OLED_SetPosition(unsigned char x, unsigned char y)
{
	HAL_OLED_WriteCommand(SSD1331_CMD_SETCOLUMN);
	HAL_OLED_WriteCommand(x);
	HAL_OLED_WriteCommand(DISPLAYWIDTH_OLED-1);

	HAL_OLED_WriteCommand(SSD1331_CMD_SETROW);
	HAL_OLED_WriteCommand(y);
	HAL_OLED_WriteCommand(DISPLAYHEIGHT_OLED-1);
}
void DRL_OLED_PushColor(uint16_t color)
{
	HAL_OLED_WriteData(color >> 8);
	HAL_OLED_WriteData(color);
}



