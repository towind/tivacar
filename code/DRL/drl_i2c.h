/*
 * drl_i2c.h
 *
 *  Created on: Mar 28, 2017
 *      Author: daniel
 */

#ifndef DRL_DRL_I2C_H_
#define DRL_DRL_I2C_H_

//register adresses of sensor

#define ACCX_H 0x3B
#define ACCX_L 0x3C
#define ACCY_H 0x3D
#define ACCY_L 0x3E
#define ACCZ_H 0x3F
#define ACCZ_L 0x40

#define TEMP_H 0x41
#define TEMP_L 0x42

#define GYRX_H 0x43
#define GYRX_L 0x44
#define GYRY_H 0x45
#define GYRY_L 0x46
#define GYRZ_H 0x47
#define GYRZ_L 0x48⁠⁠⁠⁠


#include <HAL/hal_i2c.h>
void DRL_InitMotionSens(void);
void DRL_ReadMotionSensorData(void);


#endif /* DRL_DRL_I2C_H_ */
