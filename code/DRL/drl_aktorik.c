/*
 * driver_aktorik.c
 *
 *  Created on: 31.10.2016
 *      Author: daniel
 */
#include <DRL/drl_aktorik.h>
#include <HAL/hal_pwm.h>

void DRL_ESCSetup(void)
{


	HAL_PWMThrottleSetupPulses(MAX_THROTTLE_BPW);
	__delay_cycles(HALT_TIME_MS);

	HAL_PWMThrottleSetupPulses(MIN_THROTTLE_BPW);
	__delay_cycles(HALT_TIME_MS);

	HAL_PWMThrottleSetupPulses(MIN_THROTTLE_FPW);
	__delay_cycles(HALT_TIME_MS);

	HAL_PWMThrottleSetupPulses(MAX_THROTTLE_FPW);
	__delay_cycles(HALT_TIME_MS);

	HAL_PWMThrottleSetupPulses(MAX_BRAKE);

	HAL_PWMSetThrottle(0);
	DRL_SetSteering(0);



}

unsigned int DRL_SetSteering(int steering) // -100 = full left, 0 = middle, 100 = full right
{
	unsigned int steering_conv = 0;
	//converts "steering" from AL to range of HAL PWM steering servo output
	if(steering < -100)
		steering = -100;
	else if (steering > 100)
		steering = 100;
	//unsigned char k = (MAX_STEERING_RIGHT - MAX_STEERING_LEFT)/200; //200 is the range from -100 to +100
	if(steering == 0)
	{
		steering_conv = STEERING_MIDDLE;
	}
	//unsigned int steering_conv = steering +100;
	else
	{
		steering_conv = (unsigned int)(steering +100)*((MAX_STEERING_RIGHT - MAX_STEERING_LEFT)/200) + MAX_STEERING_LEFT;
	}

	HAL_PWMSetSteering(steering_conv);
	return 0;
}

unsigned int DRL_SetThrottle(int throttle) // -100 = full backward 100 full forward
{
	if(throttle < -80)
		throttle = -80;
	else if (throttle > 80)
		throttle = 80;

	if(throttle < 0) //backwards
	{
		unsigned int throttle_conv = (unsigned int)(throttle+100)*((MIN_THROTTLE_BPW - MAX_THROTTLE_BPW)/100)+MAX_THROTTLE_BPW;
		HAL_PWMSetThrottle(throttle_conv);
	}

	else if(throttle > 0) //forwards
	{
		unsigned int throttle_conv = (unsigned int)(throttle)*((MAX_THROTTLE_FPW - MIN_THROTTLE_FPW)/100)+MIN_THROTTLE_FPW;
		HAL_PWMSetThrottle(throttle_conv);
	}
	return 0;
}

