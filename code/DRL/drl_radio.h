/*
 * drl_radio.h
 *
 *  Created on: Apr 26, 2017
 *      Author: daniel
 */

#ifndef DRL_DRL_RADIO_H_
#define DRL_DRL_RADIO_H_

#include <HAL/hal_uart.h>
void DRL_InitRFModule();
void DRL_radioRX();
void DRL_radioTX(unsigned char len);

#endif /* DRL_DRL_RADIO_H_ */
